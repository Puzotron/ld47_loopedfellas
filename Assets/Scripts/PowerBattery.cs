﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class PowerBattery : PausableMonoBehaviour {
    
    [Header("UI")]
    [SerializeField] private FillBar powerBar;
    [SerializeField] private Text powerText;

    [SerializeField] private RunningWheel runningWheel;

    public Action<float> EnergyLevelChanged;

    public float EnergyLevel { get; private set; } = 50;

    protected void Start( ) {
        Run.Every( 0f, 0.25f, () => {
            if ( !Paused ) {
                var decayValue = CalculateEnergyDecayValue();

                // Debug.Log( "Decay value " + decayValue + " " + ( runningWheel.Speed * 0.55f ) + " " + (decayValue - runningWheel.Speed * 0.55f) );
                EnergyLevel -= CalculateEnergyDecayValue(); // It will make it harder to load fully, but also slighly easier to come back
                EnergyLevel += runningWheel.Speed * 0.55f;

                if ( EnergyLevel >= 100f ) {
                    EnergyLevel = 100f;
                }

                powerBar.FillValue( EnergyLevel / 100f );
                powerText.text = Mathf.RoundToInt( EnergyLevel ).ToString() + "%";

                EnergyLevelChanged?.Invoke( EnergyLevel );
            }
        } );
    }

    protected float CalculateEnergyDecayValue( ) {
        return EnergyLevel > 65 ? EnergyLevel / 10f / 3.35f : Mathf.Max( 1f, EnergyLevel / 10f / 4f );
    }

}
