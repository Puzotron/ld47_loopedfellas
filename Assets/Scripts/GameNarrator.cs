﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using DG.Tweening;

// Lol, I a really bad designed, fat, super class for everything xD
public class GameNarrator : MonoBehaviour {

    [SerializeField] private EndPanel endingPanel;

    [Header("Camera")]
    [SerializeField] private CameraController cameraController;

    [SerializeField] private SmokeGenerator[] smokeGenerators;
    [SerializeField] private SmokeGenerator[] otherSmokeGenerators;

    [Header("Animations")]
    [SerializeField] private AnimationCurve dropCurve;

    [Header("Misc")]
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private GameObject goodFoodPrefab;
    [SerializeField] private GameObject shitFoodPrefab;
    [SerializeField] private Player currentPlayer;
    [SerializeField] private RunningWheelContainer runningWheelContainer;
    [SerializeField] private RunningWheel runningWheel;
    [SerializeField] private PowerBattery powerBattery;
    [SerializeField] private ThomasTheTrain shockThomas;
    [SerializeField] private ThomasTheTrain foodThomas;
    [SerializeField] private Bzzzzt bzzzzt;

    [Header("Controls")]
    [SerializeField] private ClickArea clickArea;

    [Header("Points")]
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private Transform targetPoint;

    // UI BARS
    [Header("UI Bars")]
    [SerializeField] private FillBar vitalityBar;
    [SerializeField] private FillBar energyBar;
    [Header("UI Stats")]
    [SerializeField] private Text enduranceText;
    [SerializeField] private Text speedText;
    [SerializeField] private Text resistanceText;

    [Header("Texts")]
    [SerializeField] private Text fellaNrText;
    [SerializeField] private Text youDiedText;
    [SerializeField] private Text welcomeText;
    [SerializeField] private Text timeToFeedText;

    [SerializeField] private GameObject statsObj;
    [SerializeField] private Image alertImage;
    [SerializeField] private FrontTv frontTv;

    [SerializeField] private GameObject wheelLegs;

    [SerializeField] private AudioSource audioPlayer;
    [SerializeField] private AudioSource alarmPlayer;

    [Header("Audio clips")]
    [SerializeField] private AudioClip weeeeeClip;
    [SerializeField] private AudioClip bamClip;
    [SerializeField] private AudioClip nomnomClip;
    [SerializeField] private AudioClip beepbeepClip;
    [SerializeField] private AudioClip alarmClip;

    public System.Action PlayerFed;

    private bool paused = false;

    public Player Player { get => currentPlayer; }
    public RunningWheel Wheel { get => runningWheel; }

    public static int MatureCount { get; set; } = 2;

    public bool Pause { 
        get => paused; 
        set {
            paused = true;
            currentPlayer.Paused = paused;
            runningWheel.Paused = paused;
            powerBattery.Paused = paused;
        } }

    public bool PlayerUnderPerforming   { get; private set; } = false;
    public bool IsPlayerAGoodBoy        { get; private set; } = false;

    public bool IsPunishmentIncoming    { get; private set; } = false;

    public int FellaNumber { get; set; } = 0;

    private bool AlertEnabled { get; set; } = false;

    private int TimeToFeed { get; set; } = 15;
    private Tween AlertTween;

    protected void Awake( ) {
        clickArea.Clicked += correct => {
            if ( correct ) {
                currentPlayer.Speed += 0.5f;
                runningWheelContainer.Shake( 0.25f );
            } else {
                currentPlayer.Speed -= 1f;

                currentPlayer.Fall( 10f );
                cameraController.Shake( 0.5f, 1.5f, 0.2f );
            }
        };

        PlayerFed += () => {
            StartCoroutine( GiveThatBitchFood() );
        };

        // Blink tween
        AlertTween = alertImage.DOFade( 0.25f, 1f )
                     .SetLoops( -1, LoopType.Yoyo )
                     .Pause();
    }

    protected void Start( ) {
        powerBattery.Paused = true;
        runningWheel.Speed = 0f;

        powerBattery.EnergyLevelChanged += energyLevel => {
            PlayerUnderPerforming = energyLevel <= 30f;
            IsPlayerAGoodBoy = energyLevel >= 65f;

            if ( energyLevel >= 100f ) {
                FinishGame( true );
            }

            if ( energyLevel <= 0f ) {
                FinishGame( false );
            }

            if ( energyLevel >= 80f ) {
                if ( !AlertEnabled ) {
                    AlertTween.Restart();
                    alarmPlayer.Play();
                }
                AlertEnabled = true;
                frontTv.Show( "SLOW DOWN !!!" );
            } else if ( energyLevel <= 20f ) {
                if ( !AlertEnabled ) {
                    AlertTween.Restart();
                    alarmPlayer.Play();
                }

                AlertEnabled = true;
                frontTv.Show( "SPEED UP !!!" );
            } else {
                AlertEnabled = false;
                alarmPlayer.Stop();
                frontTv.Hide( "THX, BYE" );
                AlertTween.Rewind();
                AlertTween.Pause();
            }
        };

        SpawnNewPlayer( 2, 2, 2 );

        StartCoroutine( GiveThatBitchFood() );
    }

    protected void Update( ) {
        if ( PlayerUnderPerforming && !IsPunishmentIncoming ) {
            StartCoroutine( GiveThatBitchPunishment() );
        }
    }

    protected void SetupPlayerEvent( Player player ) {
        currentPlayer = player;

        currentPlayer.Character.EnergyChanged += energy => {
            energyBar.FillValue( energy / 100f );
        };

        currentPlayer.Character.VitalityChanged += vitality => {
            vitalityBar.FillValue( vitality / 100f );
        };

        currentPlayer.CurrentSpeedChanged += currentSpeed => {
            Debug.Log( "CURRENT SPEED " + currentSpeed );
            runningWheel.Speed = currentSpeed;
        };

        currentPlayer.AboutToDie += ( ) => {
            powerBattery.Paused = true;

            statsObj.SetActive( false );
            youDiedText.gameObject.SetActive( true );

            Run.After( 0.75f, ( ) => { if ( currentPlayer == null ) { SpawnNewPlayer( currentPlayer.Character.Endurance,
                                                       currentPlayer.Character.Speed, 
                                                       currentPlayer.Character.Resistance ); } } );
        };

        currentPlayer.StatsChanged += ( ) => {
            enduranceText.text = currentPlayer.Character.Endurance.ToString();
            speedText.text = currentPlayer.Character.Speed.ToString();
            resistanceText.text = currentPlayer.Character.Resistance.ToString();
        };

        // Trigger important events
        currentPlayer.Character.EnergyChanged?.Invoke(currentPlayer.Character.Energy);
        currentPlayer.Character.VitalityChanged?.Invoke(currentPlayer.Character.Vitality);
    }

    protected void SpawnNewPlayer( int endurance, int speed, int resistance ) {
        var newPlayer = Instantiate( playerPrefab, spawnPoint.position, Quaternion.identity, runningWheelContainer.transform ).GetComponent<Player>();

        bzzzzt.CurrentPlayer = newPlayer;

        FellaNumber++;

        newPlayer.InitalizeWith( new Character( endurance,
                                                speed,
                                                resistance ) );

        SetupPlayerEvent( newPlayer );

        fellaNrText.text = "Fella #" + FellaNumber.ToString();

        Run.After( 0.5f, () => {
            youDiedText.gameObject.SetActive( false );
            welcomeText.gameObject.SetActive( true );
        } );

        audioPlayer.PlayOneShot( weeeeeClip, 0.5f );

        newPlayer.transform.DOMove( targetPoint.position, 1.5f )
                           .SetEase( dropCurve )
                           .OnComplete( ( ) => {
                               newPlayer.IsReadyToGo = true;

                               newPlayer.Speed = runningWheel.Speed;

                               powerBattery.Paused = false;

                               newPlayer.Animator.SetBool( "InitialFall", true );
                               cameraController.Shake( 0.5f, 1.5f );

                               enduranceText.text = newPlayer.Character.Endurance.ToString();
                               speedText.text = newPlayer.Character.Speed.ToString();
                               resistanceText.text = newPlayer.Character.Resistance.ToString();

                               audioPlayer.PlayOneShot( bamClip, 0.5f );

                               Run.After( 1f, ( ) => {
                                   welcomeText.gameObject.SetActive( false );
                                   statsObj.SetActive( true );
                               } );
                           } );
    }

    protected void FinishGame( bool won ) {
        Pause = true;

        if ( won ) {
            cameraController.Shake( 0.25f, 1f );
            foreach ( var smokeGenerator in smokeGenerators ) {
                smokeGenerator.Play();
            }

            wheelLegs.SetActive( false );

            runningWheelContainer.DropAndRunAway();

            Run.After( 3f, () => {
                currentPlayer.ShutUp = true;
                alarmPlayer.Stop();
                endingPanel.Show( true );
                Pause = true;
            } );
        } else {
            cameraController.Shake( 0.25f, 1f );

            var index = 0;
            foreach ( var smokeGenerator in smokeGenerators ) {
                Run.After( index * 0.35f, () => {
                    smokeGenerator.Play();
                    cameraController.Shake( 0.25f, 0.75f );
                } );

                index++;
            }

            var index2 = 0;
            foreach ( var otherSmokeGenerator in otherSmokeGenerators ) {
                Run.After( 0.2f + index2 * 0.3f, () => {
                    otherSmokeGenerator.Play();
                    cameraController.Shake( 0.25f, 0.75f );
                } );

                index2++;
            }

            Run.After( 1.25f, ( ) => {
                currentPlayer.ShutUp = true;
                alarmPlayer.Stop();
                endingPanel.Show( false );
                Pause = true;
            } );
        }
    }

    void PlayBamSoundFewTimes( ) {
        audioPlayer.PlayOneShot( bamClip );

        Run.After( 0.25f, () => {
            audioPlayer.PlayOneShot( bamClip, 1f );
        } );

        Run.After( 0.4f, () => {
            audioPlayer.PlayOneShot( bamClip, 0.5f );
        } );
    }

    protected IEnumerator GiveThatBitchPunishment() {
        if ( Pause ) {
            yield break;;
        }

        IsPunishmentIncoming = true;

        shockThomas.EnterScene( () => {} ); 

        yield return new WaitForSeconds( 2f );

        if ( Pause ) {
            yield break;;
        }

        audioPlayer.PlayOneShot( beepbeepClip, 1f );
        yield return new WaitForSeconds( 0.25f );

        if ( Pause ) {
            yield break;;
        }

        currentPlayer.GetDamage( 50f, 100f );
        currentPlayer.Speed += 2;
        bzzzzt.Bzt();
        cameraController.Shake( 0.5f, 1.5f );

        yield return new WaitForSeconds( 1f );

        shockThomas.ExitScene( () => {} ); 

        yield return new WaitForSeconds( 10f );

        IsPunishmentIncoming = false;

        yield return null;
    }

    protected IEnumerator GiveThatBitchFood( ) {
        if ( Pause ) {
            yield break;
        }

        while ( TimeToFeed > 0 ) {
            TimeToFeed--;
            timeToFeedText.text = TimeToFeed.ToString();
            yield return new WaitForSeconds( 1f );
        }

        foodThomas.EnterScene( () => {} ); 

        yield return new WaitForSeconds( 2f );

        if ( Pause ) {
            yield break;;
        }

        audioPlayer.PlayOneShot( beepbeepClip, 1f );

        yield return new WaitForSeconds( 0.25f );

        if ( Pause ) {
            yield break;;
        }

        DropFood();

        yield return new WaitForSeconds( 0.75f );

        if ( Pause ) {
            yield break;;
        }

        currentPlayer.GetFood( IsPlayerAGoodBoy ? 100 : 50 );

        yield return new WaitForSeconds( 1f );

        foodThomas.ExitScene( ( ) => { } );

        timeToFeedText.text = TimeToFeed.ToString();
        TimeToFeed = 15;

        PlayerFed?.Invoke();

        yield return null;
    }

    protected void DropFood( ) {
        var food = Instantiate( IsPlayerAGoodBoy ? goodFoodPrefab : shitFoodPrefab, spawnPoint.position, Quaternion.identity, runningWheelContainer.transform );
        
        food.GetComponent<SpriteRenderer>().DOFade( 0f, 0.25f ).SetDelay( 1.25f );
        food.transform.DOMove( targetPoint.position, 1.5f )
                      .SetEase( dropCurve )
                      .OnComplete( () => {
                          audioPlayer.PlayOneShot( nomnomClip, 0.5f );
                          Destroy( food );
                      } );
    }
}
