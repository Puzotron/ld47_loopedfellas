﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class Popup : MonoBehaviour {

    protected void Start( ) {
        transform.DOMoveY( transform.position.y + 4f, 4f ).OnComplete( () => Destroy( gameObject ) );
        GetComponent<SpriteRenderer>().DOFade( 0f, 1f ).SetDelay( 2.5f );
    }

}
