﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class RunningWheelContainer : MonoBehaviour {

    [SerializeField] private AnimationCurve dropCurve;
    [SerializeField] private AnimationCurve runAwayCurve;

    [SerializeField] private AudioSource hitSound;

    private bool RunningAway        { get; set; } = false;
    private Vector3 InitialPosition { get; set; }

    public void DropAndRunAway( ) {
        RunningAway = true;
        
        transform.DOMoveY( transform.position.y - 1.5f, 0.75f )
                 .SetEase( dropCurve )
                 .OnComplete( ( ) => {
                     transform.DOMoveX( transform.position.x + 12f, 2f )
                             .SetEase( runAwayCurve );
                 } );
    }

    public void Shake( float power = 0.075f ) {
        if ( !RunningAway ) {
            hitSound.Play();
            transform.DOKill();
            transform.position = InitialPosition;
            transform.DOShakePosition( 0.25f, power );
        }
    }

    protected void Awake() {
        InitialPosition = transform.position;
    }
}
