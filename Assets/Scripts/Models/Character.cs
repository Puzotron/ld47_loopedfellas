﻿using System;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    public Action<float> EnergyChanged;
    public Action<float> VitalityChanged;

    public Action<int> AgeChanged;

    // Health parameters
    public float Energy   { get; set; } = 100;
    public float Vitality { get; set; } = 100;

    public int Age { get; set; } = 0;

    // Character statistics
    public int Endurance    { get; set; } = 1;
    public int Speed        { get; set; } = 1;
    public int Resistance   { get; set; } = 1;

    public Character( int endurance = 1, int speed = 1, int resistance = 1 ) {
        var maxValue = Math.Max( endurance, Math.Max( speed, resistance ) );

        // Keep one max value, from previous character, the rest should use base
        if ( maxValue == endurance ) {
            Endurance = endurance;
            Speed = GameNarrator.MatureCount;
            Resistance = GameNarrator.MatureCount;
        } else if ( maxValue == speed ) {
            Speed = speed;
            Endurance = GameNarrator.MatureCount;
            Resistance = GameNarrator.MatureCount;
        } else if ( maxValue == resistance ) {
            Resistance = resistance;
            Endurance = GameNarrator.MatureCount;
            Speed = GameNarrator.MatureCount;
        }
    }
}
