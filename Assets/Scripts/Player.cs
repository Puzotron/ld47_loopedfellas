﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class Player : PausableMonoBehaviour {

    [SerializeField] private float currentSpeed = 1;
    [SerializeField] private AnimatorOverrideController mediumOverrideController;
    [SerializeField] private AnimatorOverrideController oldOverrideController;

    [SerializeField] private ParticleSystem smokeEffect;

    [SerializeField] private AudioSource tupPlayer;
    [SerializeField] private AudioSource hurtPlayer;
    [SerializeField] private AudioSource hehePlayer;
    [SerializeField] private AudioSource puffPlayer;

    [SerializeField] private AudioClip fallBaby;
    [SerializeField] private AudioClip dieBaby;

    [SerializeField] private AudioClip fallMedium;
    [SerializeField] private AudioClip dieMedium;

    [SerializeField] private AudioClip fallOld;
    [SerializeField] private AudioClip dieOld;

    [SerializeField] private AudioClip heheMedium;
    [SerializeField] private AudioClip heheOld;

    [SerializeField] private GameObject vitDown;
    [SerializeField] private GameObject energyDown;
    [SerializeField] private GameObject enduranceUp;
    [SerializeField] private GameObject speedUp;
    [SerializeField] private GameObject resistanceUp;

    public Action StatsChanged;

    public Action<float> CurrentSpeedChanged;
    public Action AboutToDie;

    public float MaxEnergy { get; private set; } = 100f;
    public float MaxHealth { get; private set; } = 100f;

    public float Speed {
        get => currentSpeed;
        set {
            currentSpeed = value;

            if ( currentSpeed > MaxSpeed ) {
                currentSpeed = MaxSpeed;
            } else if ( currentSpeed < 1 ) {
                currentSpeed = 1;
            }

            Animator.speed = currentSpeed / 1.5f;

            CurrentSpeedChanged?.Invoke( currentSpeed );
        }
    }

    public Character Character { get; private set; }
    public bool IsReadyToGo { get; set; } = false;
    public bool IsAlive { get => Character != null && Character.Energy > 0 && Character.Vitality > 0; }

    public Animator Animator { get; private set; }

    private float MaxSpeed { get => Character.Speed; }
    private List<Run> Runners { get; set; } = new List<Run>();

    public bool ShutUp { get; set; } = false;

    public void InitalizeWith( Character character ) {
        Character = character;
    }

    public void Die( ) {
        Debug.Log("::DIE::");
        Debug.Log(UnityEngine.StackTraceUtility.ExtractStackTrace());

        foreach ( var runner in Runners ) {
            runner.Abort();
        }

        AboutToDie?.Invoke();

        Paused = true;

        Animator.SetBool( "Dead", true );
        Animator.SetTrigger( "Fall" );

        PlayDieSound();

        transform.DOMoveY( -6, 0.5f ).OnComplete( ( ) => {
            Destroy( gameObject );
        } );
    }

    public void GetDamage( float damage, float energyBoost = 0f ) {
        Character.Vitality -= Mathf.Max( 0f, damage - Character.Resistance * 2f );
        Character.VitalityChanged( Character.Vitality );

        Character.Energy += energyBoost;

        if ( Character.Energy >= MaxEnergy ) {
            Character.Energy = MaxEnergy;
        }

        if ( Character.Energy <= 0 ) {
            Character.Energy = 0;
        }

        Character.EnergyChanged?.Invoke( Character.Energy );

        if ( !IsAlive ) {
            Camera.main.GetComponent<CameraController>().Shake( 0.5f, 1f );
            Debug.Log( "::GetDamage::DIE" );
            Die();
        }
    }

    public void Tuping( ) {
        if ( !ShutUp ) {
            tupPlayer.Play();
        }
    }

    public void GetFood( float value ) {
        Character.Energy += value;

        if ( Character.Energy > MaxEnergy ) {
            Character.Energy = MaxEnergy;
        }

        Character.EnergyChanged( Character.Energy );
    }

    public void Fall( float energyPenalty = 0f ) {
        Animator.SetTrigger( "Fall" );
        
        GetDamage( 25f );

        Character.Energy -= energyPenalty;

        if ( Character.Energy >= MaxEnergy ) {
            Character.Energy = MaxEnergy;
        }

        if ( Character.Energy <= 0 ) {
            Character.Energy = 0;
        }

        Character.EnergyChanged?.Invoke( Character.Energy );

        if ( !IsAlive ) {
            Camera.main.GetComponent<CameraController>().Shake( 0.5f, 1f );
            Debug.Log( "::Fall::DIE" );
            Die();
        } else {
            PlayFallSound();
        }
    }

    protected void PlayFallSound( ) {
        if ( Character.Age < 15 ) {
            hurtPlayer.PlayOneShot( fallBaby, 0.75f );
        }

        if ( Character.Age >= 15 && Character.Age <= 45 ) {
            hurtPlayer.PlayOneShot( fallMedium, 0.75f );

        }

        if ( Character.Age >= 45 ) {
            hurtPlayer.PlayOneShot( fallOld, 0.75f );
        }
    }

    protected void PlayDieSound( ) {
        if ( Character.Age < 15 ) {
            hurtPlayer.PlayOneShot( dieBaby, 0.75f );
        }

        if ( Character.Age >= 15 && Character.Age <= 45 ) {
            hurtPlayer.PlayOneShot( dieMedium, 0.75f );

        }

        if ( Character.Age >= 45 ) {
            hurtPlayer.PlayOneShot( dieOld, 0.75f );
        }
    }

    protected void Awake( ) {
        Animator = GetComponent<Animator>();
     
        Runners.Add( Run.Every( 0f, 0.25f, ( ) => {
            if ( IsAlive && IsReadyToGo && !Paused ) {
                Character.Energy -= 0.6f * currentSpeed - Character.Endurance * 0.15f;
                Character.EnergyChanged?.Invoke( Character.Energy );

                if (!IsAlive) {
                    Camera.main.GetComponent<CameraController>().Shake( 0.5f, 1f );
                    Debug.Log( "::Awake::RunEvery1::DIE" );
                    Die();
                }
            }
        } ) );

        Runners.Add( Run.Every( 0f, 1f, ( ) => {
            if ( IsAlive && IsReadyToGo && !Paused ) {
                Character.Age += 1;

                Character.AgeChanged?.Invoke( Character.Age );

                if ( Character.Age == 15 ) {
                    smokeEffect.Play();
                    puffPlayer.Play();
                    hehePlayer.PlayOneShot( heheMedium , 1f );

                    GameNarrator.MatureCount += 1;

                    UpdateRandomStat();
                    Run.After( 0.25f, ( ) => {
                        UpdateRandomStat();
                    } );

                    Animator.runtimeAnimatorController = mediumOverrideController;
                }

                if ( Character.Age == 40 ) {
                    smokeEffect.Play();
                    puffPlayer.Play();
                    hehePlayer.PlayOneShot( heheOld , 1f );

                    UpdateRandomStat();
                    Run.After( 0.25f, () => { DecreaseHealthAndVitality(); } );

                    Animator.runtimeAnimatorController = oldOverrideController;
                }

                if ( Character.Age == 55 ) {
                    Camera.main.GetComponent<CameraController>().Shake( 0.5f, 1f );
                    Debug.Log( "::Awake::RunEvery::Age::DIE" );
                    Die();
                }
            }
        } ) );

        Runners.Add( Run.Every( 0f, 0.25f, ( ) => {
            if ( Character != null && Animator != null && !Paused) {
                if ( Speed > 1f ) {
                    Speed -= 0.05f;
                }

                if ( Speed > 2f ) {
                    Animator.SetBool( "Sprinting", true );
                } else {
                    Animator.SetBool( "Sprinting", false );
                }
            }
        } ) );
    }

    protected void UpdateRandomStat() {
        var value = UnityEngine.Random.Range( 1, 4 );

        if ( value == 1 ) {
            Character.Endurance += 1;

            Instantiate( enduranceUp, new Vector3( transform.position.x, transform.position.y + 2.5f, transform.position.z ), Quaternion.identity, transform.parent );
        } else if ( value == 2 ) {
            Character.Speed += 1;

            Instantiate( speedUp, new Vector3( transform.position.x, transform.position.y + 2.5f, transform.position.z ), Quaternion.identity, transform.parent );
        } else if ( value >= 3 ) {
            Character.Resistance += 1;

            Instantiate( resistanceUp, new Vector3( transform.position.x, transform.position.y + 2.5f, transform.position.z ), Quaternion.identity, transform.parent );
        }

        StatsChanged?.Invoke();
    }

    protected void DecreaseHealthAndVitality( ) {
        MaxHealth = 50f;
        MaxEnergy = 50f;

        if ( Character.Vitality > MaxHealth ) {
             Character.Vitality  = MaxHealth;
        }

        if ( Character.Energy > MaxEnergy ) {
            Character.Energy = MaxEnergy;
        }

        Character.EnergyChanged?.Invoke( Character.Energy );
        Character.VitalityChanged?.Invoke( Character.Vitality );

        Instantiate( energyDown, new Vector3( transform.position.x, transform.position.y + 2.5f, transform.position.z ), Quaternion.identity, transform.parent );
        Run.After( 0.25f, () => {
            Instantiate( vitDown, new Vector3( transform.position.x, transform.position.y + 2.5f, transform.position.z ), Quaternion.identity, transform.parent );
        } );


        StatsChanged?.Invoke();
    }
}
