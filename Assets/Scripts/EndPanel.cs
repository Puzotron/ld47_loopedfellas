﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

 using UnityEngine.SceneManagement;

using DG.Tweening;

public class EndPanel : MonoBehaviour {

    [SerializeField] Image panelImage;
    [SerializeField] Button restartButton;

    [SerializeField] GameObject goodText;
    [SerializeField] GameObject badText;

    public void Show( bool goodEnding ) {
        restartButton.gameObject.SetActive( true );
        panelImage.DOFade( 1f, 0.5f );

        if ( goodEnding ) {
            goodText.SetActive( true );
        } else {
            badText.SetActive( true );
        }
    }

    public void PlayAgain( ) {
         SceneManager.LoadScene( SceneManager.GetActiveScene().name );
    }

}
