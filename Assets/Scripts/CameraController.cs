﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class CameraController : MonoBehaviour {
    public void Shake( float duration, float strength, float delay = 0f ) {
        transform.DOKill();
        transform.DOShakePosition( duration, strength ).SetDelay( delay );
    }
}
