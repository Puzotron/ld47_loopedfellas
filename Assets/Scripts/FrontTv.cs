﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class FrontTv : MonoBehaviour {

    [SerializeField] private Text alertTextUi;

    public void Show( string alertText ) {
        alertTextUi.text = alertText;

        transform.DOMoveY( 0.5f, 0.5f );
    }

    public void Hide( string alertText ) {
        alertTextUi.text = alertText;

        transform.DOMoveY( 3.5f, 1.5f );
    }

}
