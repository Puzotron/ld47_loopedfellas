﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillBar : MonoBehaviour {
    [SerializeField] private Image fillImage;
    
    public void FillValue(float value) {
        fillImage.fillAmount = value;
    }
}
