﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeGenerator : MonoBehaviour {
    [SerializeField] private ParticleSystem system;
    [SerializeField] private AudioSource bamSource;

    public void Play( ) {
        system.Play();
        
        bamSource.Play();
    }

}
