
public class SimpleEvent {

    private System.Action eventAction = ()=> {};

    public void Add( System.Action aDelegate ) => eventAction += aDelegate;

    public void Remove( System.Action aDelegate ) => eventAction -= aDelegate;

    public void Run( ) => eventAction?.Invoke();
}
