using System.Linq;
using System.Reflection;

using UnityEditor;

public class GUIHelper {
    private static int winIDCounter = 1340;

    public static int GetFreeWindowID( ) {
        return winIDCounter++;
    }
}

