using UnityEngine;

public class MonoBehaviourSingleton<TSelfType> : MonoBehaviour where TSelfType : MonoBehaviour {
    private static TSelfType instance = null;

    public static TSelfType Instance {
        get {
            if ( instance == null ) {
                instance = ( TSelfType ) FindObjectOfType( typeof( TSelfType ) );

                if ( instance == null ) {
                    instance = new GameObject( typeof( TSelfType ).Name ).AddComponent<TSelfType>();
                }
                DontDestroyOnLoad( instance.gameObject );
            }

            return instance;
        }
    }
}

