using UnityEngine;
using System.Collections;

public class Run {

    private IEnumerator action;

    public System.Action onGUIaction = null;

    public bool IsDone { get; set; }
    public bool Aborted { get; set; }

    public Coroutine WaitingFor {
        get => CoroutineHelper.Instance.StartCoroutine( WaitFor( null ) );
    }

    public static CTempWindow CreateGUIWindow( Rect pos, string title, System.Action<CTempWindow> action ) {
        var tmp = new CTempWindow {
            pos = pos,
            title = title
        };

        tmp.inst = OnGUI( 0, ( ) => {
            tmp.pos = UnityEngine.GUI.Window( tmp.winID, tmp.pos, ( id ) => {
                action( tmp );
            }, tmp.title );
        } );

        return tmp;
    }

    public static Run EachFrame( System.Action aAction ) {
        var tmp = new Run();

        tmp.action = RunEachFrame( tmp, aAction );
        tmp.Start();

        return tmp;
    }

    public static Run Every( float initialDelay, float delay, System.Action action ) {
        var tmp = new Run();

        tmp.action = RunEvery( tmp, initialDelay, delay, action );
        tmp.Start();

        return tmp;
    }

    public static Run After( float delay, System.Action action ) {
        var tmp = new Run();

        tmp.action = RunAfter( tmp, delay, action );
        tmp.Start();

        return tmp;
    }

    public static Run Lerp( float duration, System.Action<float> action ) {
        var tmp = new Run();

        tmp.action = RunLerp( tmp, duration, action );
        tmp.Start();

        return tmp;
    }

    public static Run OnDelegate( SimpleEvent simpleEvent, System.Action action ) {
        var tmp = new Run();

        tmp.action = RunOnDelegate( tmp, simpleEvent, action );
        tmp.Start();

        return tmp;
    }

    public static Run Coroutine( IEnumerator coroutine ) {
        var tmp = new Run();

        tmp.action = RunCoroutine( tmp, coroutine );
        tmp.Start();

        return tmp;
    }

    public static Run OnGUI( float duration, System.Action action ) {
        var tmp = new Run {
            onGUIaction = action
        };

        if ( duration > 0.0f ) {
            tmp.action = RunAfter( tmp, duration, null );
        } else {
            tmp.action = null;
        }

        tmp.Start();

        CoroutineHelper.Instance.Add( tmp );

        return tmp;
    }

    private static IEnumerator RunEachFrame( Run runner, System.Action action ) {
        runner.IsDone = false;

        while ( true ) {
            if ( !runner.Aborted && action != null )
                action();
            else
                break;

            yield return null;
        }

        runner.IsDone = true;
    }

    private static IEnumerator RunEvery( Run runner, float initialDelay, float seconds, System.Action action ) {
        runner.IsDone = false;

        if ( initialDelay > 0f )
            yield return new WaitForSeconds( initialDelay );
        else {
            var FrameCount = Mathf.RoundToInt( initialDelay);

            for ( int i = 0; i < FrameCount; i++ ) {
                yield return null;
            }
        }

        while ( true ) {
            if ( !runner.Aborted && action != null ) {
                action();
            } else {
                break;
            }

            if ( seconds > 0 ) {
                yield return new WaitForSeconds( seconds );
            } else {
                var frameCount = Mathf.Max( 1, Mathf.RoundToInt( -seconds ) );

                for ( int i = 0; i < frameCount; i++ ) {
                    yield return null;
                }
            }
        }

        runner.IsDone = true;
    }

    private static IEnumerator RunAfter( Run runner, float delay, System.Action action ) {
        runner.IsDone = false;

        yield return new WaitForSeconds( delay );

        if ( !runner.Aborted && action != null ) {
            action();
        }

        runner.IsDone = true;
    }

    private static IEnumerator RunLerp( Run runner, float duration, System.Action<float> action ) {
        runner.IsDone = false;

        var t = 0f;

        while ( t < 1.0f ) {
            t = Mathf.Clamp01( t + Time.deltaTime / duration );

            if ( !runner.Aborted && action != null ) {
                action( t );
            }

            yield return null;
        }

        runner.IsDone = true;
    }

    private static IEnumerator RunOnDelegate( Run runner, SimpleEvent simpleEvent, System.Action action ) {
        runner.IsDone = false;

        void act( ) => action();
        simpleEvent.Add( act );

        while ( !runner.Aborted && action != null ) {
            yield return null;
        }

        simpleEvent.Remove( action );
        runner.IsDone = true;
    }

    private static IEnumerator RunCoroutine( Run runner, IEnumerator coroutine ) {
        yield return CoroutineHelper.Instance.StartCoroutine( coroutine );

        runner.IsDone = true;
    }

    public void Abort( ) {
        Aborted = true;
    }

    public IEnumerator WaitFor( System.Action onDone ) {
        while ( !IsDone ) {
            yield return null;
        }

        onDone?.Invoke();
    }

    public Run ExecuteWhenDone( System.Action action ) {
        var tmp = new Run {
            action = WaitFor( action )
        };
        tmp.Start();

        return tmp;
    }

    private void Start( ) {
        if ( action != null ) {
            CoroutineHelper.Instance.StartCoroutine( action );
        }
    }

    public class CTempWindow {
        public Run      inst;
        public Rect     pos;
        public string   title;

        public int winID = GUIHelper.GetFreeWindowID();
        public void Close( ) { inst.Abort(); }
    }
}
