using System.Collections.Generic;

public class CoroutineHelper : MonoBehaviourSingleton<CoroutineHelper> {

    private List<Run> onGUIObjects = new List<Run>();

    public int ScheduledOnGUIItems { get { return onGUIObjects.Count; } }

    public Run Add( Run runner ) {
        if ( runner != null ) {
            onGUIObjects.Add( runner );
        }

        return runner;
    }

    protected void OnGUI( ) {
        foreach ( var runner in onGUIObjects ) {
            if ( !runner.Aborted && !runner.IsDone && runner.onGUIaction != null ) {
                runner.onGUIaction();
            } else {
                runner.IsDone = true;
            }
        }
    }

    protected void Update( ) {
        for ( int i = onGUIObjects.Count - 1; i >= 0; i-- ) {
            if ( onGUIObjects[ i ].IsDone )
                onGUIObjects.RemoveAt( i );
        }
    }
}
