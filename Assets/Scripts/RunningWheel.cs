﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningWheel : PausableMonoBehaviour {

    [SerializeField] ClickArea clickArea;
    [SerializeField] float speed = 1;

    public float Speed { get => speed; set => speed = value; }

    private SpriteRenderer Renderer { get; set; }

    protected void Awake( ) {
        Renderer = GetComponent<SpriteRenderer>();
    }

    protected void Update( ) {
        transform.Rotate( 0, 0, -36 * speed * 1.85f * Time.deltaTime ); // 36 degrees per second
    }

    void OnMouseDown( ) {
        clickArea.Click( false );
    }
}
