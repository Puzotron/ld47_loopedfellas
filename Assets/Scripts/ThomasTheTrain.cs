﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class ThomasTheTrain : MonoBehaviour {

    [SerializeField] private AnimationCurve xCurve;

    private float speed = 16f;
    private float height = 0.0035f;
    private bool wave = false;

    private SpriteRenderer Renderer { get; set; }

    public void EnterScene( System.Action onComplete ) {
        wave = true;

        transform.DOMoveX( 0f, 2f )
                 .SetEase( xCurve )
                 .OnComplete( () => { 
                     wave = false;
                     onComplete?.Invoke();
                  } );
    }

    public void ExitScene( System.Action onComplete ) {
        wave = true;

        transform.DOMoveX( -12f, 2f )
                 .SetEase( xCurve )
                 .OnComplete( () => { 
                     wave = false;
                     onComplete?.Invoke();
                  } );
    }

    protected void Awake( ) {
        Renderer = GetComponent<SpriteRenderer>();
    }

    protected void Update( ) {
        if ( !wave ) {
            return;
        }

        var pos = transform.position;
        var offsetY = Mathf.Sin( Time.time * speed );

        transform.position = new Vector3( pos.x, pos.y + offsetY * height, pos.z );
    }

}
