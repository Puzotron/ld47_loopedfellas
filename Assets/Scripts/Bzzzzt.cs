﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bzzzzt : MonoBehaviour {
    [SerializeField] private GameObject bztContainer;

    public Player CurrentPlayer { get; set; } 

    public void Bzt( ) {
        StartCoroutine( BztButBetter( ) );
    }

    protected IEnumerator BztButBetter( ) {
        GetComponent<AudioSource>().Play();

        if ( CurrentPlayer ) {
            bztContainer.SetActive( true );
            CurrentPlayer.GetComponent<SpriteRenderer>().color = Color.black;
        }

        yield return new WaitForSeconds( 0.05f );

        if ( CurrentPlayer ) {
            bztContainer.SetActive( false );
            CurrentPlayer.GetComponent<SpriteRenderer>().color = Color.white;
        }

        yield return new WaitForSeconds( 0.05f );

        if ( CurrentPlayer ) {
            bztContainer.SetActive( true );
            CurrentPlayer.GetComponent<SpriteRenderer>().color = Color.black;
        }

        yield return new WaitForSeconds( 0.05f );

        if ( CurrentPlayer ) {
            bztContainer.SetActive( false );
            CurrentPlayer.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

}
