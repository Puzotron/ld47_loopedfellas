﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickArea : MonoBehaviour {
    [SerializeField] private RunningWheel runningWheel;

    public System.Action<bool> Clicked;

    private bool MouseClickGuard { get; set; } = false;

    protected void OnMouseDown( ) {
        var rotation = runningWheel.transform.localRotation.z;

        if ( ( rotation > -150f && rotation > -180f ) || ( rotation < 180f && rotation > 150f ) ) {
            Click( true );
        }
    }

    public void Click( bool correctHit ) {
        if ( !MouseClickGuard ) {
            Clicked?.Invoke( correctHit );
            MouseClickGuard = true;

            Run.After( 0.25f, ( ) => { MouseClickGuard = false; } );
        }
    }
}
