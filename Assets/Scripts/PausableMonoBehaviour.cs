﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausableMonoBehaviour : MonoBehaviour
{
    public bool Paused { get; set; }
}
